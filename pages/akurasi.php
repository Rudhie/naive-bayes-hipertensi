<?php
	require_once("proses/DBConnection.php");
	$Dbobj = new DBConnection();
	$query = "SELECT id_data_uji, umur, sistol, diastol, berat_badan, kelas_awal, kelas_sistem FROM data_uji_pasien WHERE kelas_sistem IS NOT NULL ORDER BY id_data_uji DESC";
	$act = mysqli_query($Dbobj->getdbconnect(), $query);

	$queryAkurasi = "SELECT count(id_data_uji) as jml_akurasi FROM data_uji_pasien WHERE kelas_awal = kelas_sistem";
	$dataAKurasi = mysqli_fetch_row(mysqli_query($Dbobj->getdbconnect(), $queryAkurasi));

	$queryLaju = "SELECT count(id_data_uji) as jml_laju FROM data_uji_pasien WHERE kelas_awal != kelas_sistem";
	$dataLaju = mysqli_fetch_row(mysqli_query($Dbobj->getdbconnect(), $queryLaju));

	$queryLatih = "SELECT id_data_latih, umur, sistol, diastol, berat_badan, kelas FROM data_latih_pasien ORDER BY id_data_latih DESC";
	$dataLatih = mysqli_num_rows(mysqli_query($Dbobj->getdbconnect(), $queryLatih));
?>
<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>Evaluasi Sistem</h2>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<table class="table table-bordered">
					<tr>
						<th width="15%">Total Data Latih</th>
						<td><?= $dataLatih; ?></td>
						<th width="15%">Akurasi</th>
						<td><?= $dataAKurasi[0]; ?></td>
						<th width="15%">Nilai Akurasi</th>
						<td><?php echo (($dataAKurasi[0] / mysqli_num_rows($act)) * 100)."%"; ?></td>
					</tr>
					<tr>
						<th width="15%">Total Data Latih</th>
						<td><?= mysqli_num_rows($act); ?></td>
						<th width="15%">Laju Error</th>
						<td><?= $dataLaju[0]; ?></td>
						<th width="15%">Nilai Laju Error</th>
						<td><?php echo (($dataLaju[0] / mysqli_num_rows($act)) * 100)."%"; ?></td>
					</tr>
				</table>
				<hr/>
				<table id="data" class="table table-stripped" width="100%">
					<thead>
						<tr>
							<th width="5%">No</th>
							<th>Usia</th>
							<th>Sistol</th>
							<th>Diastol</th>
							<th>Berat Badan</th>
							<th>Kelas Awal</th>
							<th>Kelas Sistem</th>
							<th>Akurasi</th>
						</tr>
					</thead>
					<tbody>
						<?php $i = 1; while ($result = mysqli_fetch_assoc($act)) { ?>
						<tr>
							<td><?= $i ?></td>
							<td><?= $result["umur"] ?></td>
							<td><?= $result["sistol"] ?></td>
							<td><?= $result["diastol"] ?></td>
							<td><?= $result["berat_badan"] ?></td>
							<td><?= strtoupper($result["kelas_awal"]) ?></td>
							<td><?= strtoupper($result["kelas_sistem"]) ?></td>
							<td>
								<?php if($result["kelas_awal"] == $result["kelas_sistem"]){ echo "<span class='label label-success'>BENAR</span>"; } else { echo "<span class='label label-danger'>SALAH</span>";}  ?>
							</td>
						</tr>
						<?php $i++; } ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$("#data").dataTable();
	});

	function hapusUji(id){
		if (confirm('Apakah Yakin Ingin Menghapus Data Uji?')){
			window.location = "proses/hapus-uji.php?id="+id;
		}
	}
</script>