<div class="row">
    <div class="col-md-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Upload Data Uji</h2>&nbsp;&nbsp;
                <a href="assets/master_template_upload_data_uji.xlsx" class="btn btn-sm btn-warning">Download Template Upload Data Uji</a>
                <div class="clearfix"></div>
            </div>
            <div class="panel-body">
                <?php if(isset($_GET["sukses"])){ ?>
                    <div class="alert alert-success alert-dismissible">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong>Success!</strong> Berhasil menambahkan data uji.
                    </div>
                <?php }  else if(isset($_GET["error"])){ ?>
                    <div class="alert alert-danger alert-dismissible">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong>Error!</strong> Gagal menambahkan data uji.
                    </div>
                <?php } ?>
                <form class="form-horizontal" action="proses/proses-upload-uji.php" method="post" name="frmExcelImport" id="frmExcelImport" enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="exampleInputEmail3" class="col-sm-1 control-label">Berkas</label>
                        <div class="col-sm-4">
                            <input class="form-control" type="file" name="file" id="file" accept=".xls,.xlsx">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-1 col-sm-10">
                            <div class="checkbox">
                                <label><input type="checkbox" name="check"> Centang Jika Ingin Menghapus Data Uji Lama</label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-1 col-sm-10">
                            <input name="import" type="submit" id="submit" value="Upload Data" class="btn btn-sm btn-primary">
                            <a href="aplikasi.php?page=uji" class="btn btn-sm btn-info">Kembali</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>  
    </div>
</div>