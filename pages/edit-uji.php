<?php
    require_once("proses/DBConnection.php");
    $Dbobj = new DBConnection();
    $query = "SELECT id_data_uji, umur, sistol, diastol, berat_badan, kelas_awal FROM data_uji_pasien WHERE id_data_uji = ".$_GET['id'];
    $act = mysqli_query($Dbobj->getdbconnect(), $query);
    $result = mysqli_fetch_assoc($act);
?>
<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>Edit Data Latih Pasien</h2>&nbsp;&nbsp;
				<a href="?page=uji" class="btn btn-sm btn-success">Kembali</a>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<form action="proses/edit-uji-proses.php" method="POST" class="form-horizontal form-label-left">
                    <div class="form-group">
                        <label class="control-label col-md-2 col-sm-3 col-xs-12" for="first-name">Umur Pasien</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="hidden" name="id_data_uji" value="<?= $result['id_data_uji'] ?>">
                            <input type="number" class="form-control col-md-7 col-xs-12" value="<?= $result['umur'] ?>" name="umur" required="required">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="middle-name" class="control-label col-md-2 col-sm-3 col-xs-12">Sistol</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="number" class="form-control col-md-7 col-xs-12" value="<?= $result['sistol'] ?>" name="sistol" required="required">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="middle-name" class="control-label col-md-2 col-sm-3 col-xs-12">Diastol</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="number" class="form-control col-md-7 col-xs-12" value="<?= $result['diastol'] ?>" name="diastol" required="required">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="middle-name" class="control-label col-md-2 col-sm-3 col-xs-12">Berat Badan</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="number" class="form-control col-md-7 col-xs-12" value="<?= $result['berat_badan'] ?>" name="bb" required="required">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="middle-name" class="control-label col-md-2 col-sm-3 col-xs-12">Kelas</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <select name="kelas" class="form-control">
                            	<option value="normal" <?php if($result["kelas_awal"] == "normal"){ echo "selected";} ?>>Normal</option>
                            	<option value="prahipertensi" <?php if($result["kelas_awal"] == "prahipertensi"){ echo "selected";} ?>>Prahipertensi</option>
                            	<option value="tahap 1" <?php if($result["kelas_awal"] == "tahap 1"){ echo "selected";} ?>>Tahap 1</option>
                            	<option value="tahap 2" <?php if($result["kelas_awal"] == "tahap 2"){ echo "selected";} ?>>Tahap 2</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="middle-name" class="control-label col-md-2 col-sm-3 col-xs-12"></label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="submit" name="edit_uji" value="Simpan" class="btn btn-info" required="required">
                        </div>
                    </div>
                </form>
			</div>
		</div>
	</div>
</div>