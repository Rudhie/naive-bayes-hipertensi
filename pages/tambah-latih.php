<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>Tambah Data Latih Pasien</h2>&nbsp;&nbsp;
				<a href="?page=latih" class="btn btn-sm btn-success">Kembali</a>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<form action="proses/tambah-latih-proses.php" method="POST" class="form-horizontal form-label-left">
                    <div class="form-group">
                        <label class="control-label col-md-2 col-sm-3 col-xs-12" for="first-name">Umur Pasien</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="number" class="form-control col-md-7 col-xs-12" name="umur" required="required">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="middle-name" class="control-label col-md-2 col-sm-3 col-xs-12">Sistol</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="number" class="form-control col-md-7 col-xs-12" name="sistol" required="required">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="middle-name" class="control-label col-md-2 col-sm-3 col-xs-12">Diastol</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="number" class="form-control col-md-7 col-xs-12" name="diastol" required="required">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="middle-name" class="control-label col-md-2 col-sm-3 col-xs-12">Berat Badan</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="number" class="form-control col-md-7 col-xs-12" name="bb" required="required">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="middle-name" class="control-label col-md-2 col-sm-3 col-xs-12">Kelas</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <select name="kelas" class="form-control">
                            	<option value="normal">Normal</option>
                            	<option value="prahipertensi">Prahipertensi</option>
                            	<option value="tahap 1">Tahap 1</option>
                            	<option value="tahap 2">Tahap 2</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="middle-name" class="control-label col-md-2 col-sm-3 col-xs-12"></label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="submit" name="tambah_uji" value="Simpan" class="btn btn-info" required="required">
                        </div>
                    </div>
                </form>
			</div>
		</div>
	</div>
</div>