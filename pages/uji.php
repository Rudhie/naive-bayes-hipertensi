<?php
	require_once("proses/DBConnection.php");
	$Dbobj = new DBConnection();
	$query = "SELECT id_data_uji, umur, sistol, diastol, berat_badan, kelas_awal, kelas_sistem FROM data_uji_pasien ORDER BY id_data_uji DESC";
	$act = mysqli_query($Dbobj->getdbconnect(), $query);
?>
<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>Data Uji Pasien</h2>&nbsp;&nbsp;
				<a href="?page=tambah-uji" class="btn btn-sm btn-info">Tambah</a>
				<a href="?page=upload-uji" class="btn btn-sm btn-info">Upload Excel Data Uji</a>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<table id="data" class="table table-stripped" width="100%">
					<thead>
						<tr>
							<th width="5%">No</th>
							<th>Usia</th>
							<th>Sistol</th>
							<th>Diastol</th>
							<th>Berat Badan</th>
							<th>Kelas Awal</th>
							<th width="10%">Aksi</th>
						</tr>
					</thead>
					<tbody>
						<?php $i = 1; while ($result = mysqli_fetch_assoc($act)) { ?>
						<tr>
							<td><?= $i ?></td>
							<td><?= $result["umur"] ?></td>
							<td><?= $result["sistol"] ?></td>
							<td><?= $result["diastol"] ?></td>
							<td><?= $result["berat_badan"] ?></td>
							<td><?= $result["kelas_awal"] ?></td>
							<td>
								<a href="?page=edit-uji&id=<?= $result['id_data_uji']; ?>" class="btn btn-xs btn-warning">Ubah</a>&nbsp;
								<a onClick="hapusUji(<?= $result['id_data_uji']; ?>)" class="btn btn-xs btn-danger">Hapus</a>
							</td>
						</tr>
						<?php $i++; } ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$("#data").dataTable();
	});

	function hapusUji(id){
		if (confirm('Apakah Yakin Ingin Menghapus Data Uji?')){
			window.location = "proses/hapus-uji.php?id="+id;
		}
	}
</script>