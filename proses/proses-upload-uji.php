<?php
require_once("DBConnection.php");
$Dbobj = new DBConnection();
require_once('../vendor/php-excel-reader/excel_reader2.php');
require_once('../vendor/SpreadsheetReader.php');
if(isset($_POST["check"])){
    $sqlDelete = "DELETE FROM data_uji_pasien";
    mysqli_query($Dbobj->getdbconnect(), $sqlDelete);
}

if (isset($_POST["import"])){  
    $allowedFileType = ['application/vnd.ms-excel','text/xls','text/xlsx','application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'];

    if(in_array($_FILES["file"]["type"],$allowedFileType)){
        $targetPath = '../uploads/'.$_FILES['file']['name'];
        move_uploaded_file($_FILES['file']['tmp_name'], $targetPath);
        $Reader = new SpreadsheetReader($targetPath);
        $sheetCount = count($Reader->sheets());
        for($i=0;$i<$sheetCount;$i++){
            $Reader->ChangeSheet($i);
            foreach ($Reader as $Row){
                $umur = null;
                if(isset($Row[1]) && $Row[1] != "umur") {
                    $umur = mysqli_real_escape_string($Dbobj->getdbconnect(),$Row[1]);
                }

                $sistol = null;
                if(isset($Row[2]) && $Row[2] != "sistol") {
                    $sistol = mysqli_real_escape_string($Dbobj->getdbconnect(),$Row[2]);
                }

                $diastol = null;
                if(isset($Row[3]) && $Row[3] != "diastol") {
                    $diastol = mysqli_real_escape_string($Dbobj->getdbconnect(),$Row[3]);
                }

                $bb = null;
                if(isset($Row[4]) && $Row[4] != "berat badan") {
                    $bb = mysqli_real_escape_string($Dbobj->getdbconnect(),$Row[4]);
                }

                $kelas = null;
                if(isset($Row[5]) && $Row[1] != "kelas") {
                    $kelas = mysqli_real_escape_string($Dbobj->getdbconnect(),$Row[5]);
                }

                if (!empty($umur) || !empty($sistol) || !empty($diastol) || !empty($bb) || !empty($kelas)) {
                    $query = "INSERT INTO data_uji_pasien (umur,sistol, diastol, berat_badan,kelas_awal) values('".$umur."','".$sistol."','".$diastol."','".$bb."','".strtolower($kelas)."')";
                    $result = mysqli_query($Dbobj->getdbconnect(), $query);
                
                    if (! empty($result)) {
                        $type = "success";
                        $message = "Data Master Transaksi Berhasil Di Tambakan";
                    } else {
                        $type = "error";
                        $message = "Kesalahan Saat Menambahkan Data Baru";
                    }
                }
            }
        }
        mysqli_query($Dbobj->getdbconnect(), "DELETE FROM data_uji_pasien WHERE kelas_awal = 'kelas'");
        header("location: ".$Dbobj->baseurl."aplikasi.php?page=upload-uji&sukses=true");
    } else { 
        $type = "error";
        $message = "File Gagal Diupload, Hanya format .xls";
        header("location: ".$Dbobj->baseurl."aplikasi.php?page=upload-uji&eerror=true");
    }
}
?>