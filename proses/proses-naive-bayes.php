<?php
	require_once("DBConnection.php");
	$Dbobj = new DBConnection();
	$queryProb = "SELECT kelas, count( id_data_latih ) as jumlah FROM data_latih_pasien GROUP BY kelas";
	$totalKelas = mysqli_fetch_row(
		mysqli_query(
			$Dbobj->getdbconnect(), 
			"SELECT count( id_data_latih ) as jumlah FROM data_latih_pasien"
		)
	);

	$actProb = mysqli_query($Dbobj->getdbconnect(), $queryProb);
	while ($r = mysqli_fetch_assoc($actProb)) {
		$dataProbKelas[$r['kelas']] = $r["jumlah"] / $totalKelas[0];
	}

	$queryProbAtr = "SELECT kelas, AVG( umur ) AS rata_umur, avg( sistol ) AS rata_sistol, avg( diastol ) AS rata_diastol, avg( berat_badan ) AS rata_bb, VAR_SAMP( umur ) AS var_umur, VAR_SAMP( sistol ) AS var_sistol, VAR_SAMP( diastol ) AS var_diastol, VAR_SAMP( berat_badan ) AS var_bb, STDDEV_SAMP( umur ) AS stdv_umur, STDDEV_SAMP( sistol ) AS stdv_sistol, STDDEV_SAMP( diastol ) AS stdv_diastol, STDDEV_SAMP( berat_badan ) AS stdv_bb FROM data_latih_pasien GROUP BY kelas ORDER BY kelas";

	$actProbAtr = mysqli_query($Dbobj->getdbconnect(), $queryProbAtr);

	while ($rAtr = mysqli_fetch_assoc($actProbAtr)) {
		$dataProbAtr[$rAtr['kelas']] = $rAtr;
	}
	$queryUji = "SELECT id_data_uji, umur, sistol, diastol, berat_badan as bb FROM data_uji_pasien ORDER BY id_data_uji ASC";
	$actProbUji = mysqli_query($Dbobj->getdbconnect(), $queryUji);
	$dataUji = array();
	while($rUji = mysqli_fetch_array($actProbUji)){
		array_push($dataUji, array(
			"id_data_uji" => $rUji["id_data_uji"],
			"umur" => $rUji["umur"],
			"sistol" => $rUji["sistol"],
			"diastol" => $rUji["diastol"],
			"bb" =>$rUji["bb"]
		));
	};
	foreach ($dataUji as $dkey => $dvalue) {
		$umur = $dvalue['umur'];
		$sistol = $dvalue['sistol'];
		$diastol = $dvalue['diastol'];
		$bb = $dvalue['bb'];

		$hit["normal"]["u"] = normdist($umur, $dataProbAtr['normal']['rata_umur'], $dataProbAtr['normal']['var_umur'], $dataProbAtr['normal']['stdv_umur']);
		$hit["normal"]["s"] = normdist($sistol, $dataProbAtr['normal']['rata_sistol'], $dataProbAtr['normal']['var_sistol'], $dataProbAtr['normal']['stdv_sistol']);
		$hit["normal"]["d"] = normdist($diastol, $dataProbAtr['normal']['rata_diastol'], $dataProbAtr['normal']['var_diastol'], $dataProbAtr['normal']['stdv_diastol']);
		$hit["normal"]["b"] = normdist($bb, $dataProbAtr['normal']['rata_bb'], $dataProbAtr['normal']['var_bb'], $dataProbAtr['normal']['stdv_bb']);

		$hit["pra"]["u"] = normdist($umur, $dataProbAtr['prahipertensi']['rata_umur'], $dataProbAtr['prahipertensi']['var_umur'], $dataProbAtr['prahipertensi']['stdv_umur']);
		$hit["pra"]["s"] = normdist($sistol, $dataProbAtr['prahipertensi']['rata_sistol'], $dataProbAtr['prahipertensi']['var_sistol'], $dataProbAtr['prahipertensi']['stdv_sistol']);
		$hit["pra"]["d"] = normdist($diastol, $dataProbAtr['prahipertensi']['rata_diastol'], $dataProbAtr['prahipertensi']['var_diastol'], $dataProbAtr['prahipertensi']['stdv_diastol']);
		$hit["pra"]["b"] = normdist($bb, $dataProbAtr['prahipertensi']['rata_bb'], $dataProbAtr['prahipertensi']['var_bb'], $dataProbAtr['prahipertensi']['stdv_bb']);

		$hit["tahap 1"]["u"] = normdist($umur, $dataProbAtr['tahap 1']['rata_umur'], $dataProbAtr['tahap 1']['var_umur'], $dataProbAtr['tahap 1']['stdv_umur']);
		$hit["tahap 1"]["s"] = normdist($sistol, $dataProbAtr['tahap 1']['rata_sistol'], $dataProbAtr['tahap 1']['var_sistol'], $dataProbAtr['tahap 1']['stdv_sistol']);
		$hit["tahap 1"]["d"] = normdist($diastol, $dataProbAtr['tahap 1']['rata_diastol'], $dataProbAtr['tahap 1']['var_diastol'], $dataProbAtr['tahap 1']['stdv_diastol']);
		$hit["tahap 1"]["b"] = normdist($bb, $dataProbAtr['tahap 1']['rata_bb'], $dataProbAtr['tahap 1']['var_bb'], $dataProbAtr['tahap 1']['stdv_bb']);

		$hit["tahap 2"]["u"] = normdist($umur, $dataProbAtr['tahap 2']['rata_umur'], $dataProbAtr['tahap 2']['var_umur'], $dataProbAtr['tahap 2']['stdv_umur']);
		$hit["tahap 2"]["s"] = normdist($sistol, $dataProbAtr['tahap 2']['rata_sistol'], $dataProbAtr['tahap 2']['var_sistol'], $dataProbAtr['tahap 2']['stdv_sistol']);
		$hit["tahap 2"]["d"] = normdist($diastol, $dataProbAtr['tahap 2']['rata_diastol'], $dataProbAtr['tahap 2']['var_diastol'], $dataProbAtr['tahap 2']['stdv_diastol']);
		$hit["tahap 2"]["b"] = normdist($bb, $dataProbAtr['tahap 2']['rata_bb'], $dataProbAtr['tahap 2']['var_bb'], $dataProbAtr['tahap 2']['stdv_bb']);

		$n = $hit['normal']["u"] * $hit['normal']["s"] * $hit['normal']["d"] * $hit['normal']["b"];
		$p = $hit['pra']["u"] * $hit['pra']["s"] * $hit['pra']["d"] * $hit['pra']["b"];
		$t1 = $hit['tahap 1']["u"] * $hit['tahap 1']["s"] * $hit['tahap 1']["d"] * $hit['tahap 1']["b"];
		$t2 = $hit['tahap 2']["u"] * $hit['tahap 2']["s"] * $hit['tahap 2']["d"] * $hit['tahap 2']["b"];
		$hasilAkhir[$dvalue["id_data_uji"]] = array("normal" => $n, "prahipertensi" => $p, "tahap 1" => $t1, "tahap 2" => $t2);
	}

	foreach ($hasilAkhir as $hkey => $hvalue) {
		$queryUpdate = "UPDATE data_uji_pasien SET kelas_sistem = '".array_search(max($hasilAkhir[$hkey]), $hasilAkhir[$hkey])."' WHERE id_data_uji = ".$hkey;
		$actUpd = mysqli_query($Dbobj->getdbconnect(), $queryUpdate);
		// echo($hkey);
		// echo (array_search(max($hasilAkhir[$hkey]), $hasilAkhir[$hkey]));
		// print_r(min(array_keys($hasilAkhir[$hkey])));
	}
	// echo "<pre>";
	// print_r($hasilAkhir);
	// echo "</pre>";
	header("location: ".$Dbobj->baseurl."aplikasi.php?page=hitung");
	exit();
	$dataUji = array("umur" => 28, "sistol" => 80, "diastol" => 80, "bb" => 50);
	foreach ($dataUji as $ikey => $ivalue) {
		$probUji = array();
		foreach ($dataProbAtr as $key => $value) {
			$uji = normdist($ivalue, $value['rata_'.$ikey], $value['var_'.$ikey], $value['stdv_'.$ikey]);
			array_push($probUji, $uji);
		}
		$dataProbUji[] = $probUji;
	}

	foreach ($dataProbUji as $ukey => $uvalue) {
		echo $uvalue[0]."<br/>";
	}

	function normdist($x, $mean, $variance, $stddev){
		$val = 1 / (sqrt(2 * 3.14) * $stddev);
		$exp = exp(-(pow(($x - $mean), 2)) / (2 * $variance));

		return $val * $exp;
	}
?>